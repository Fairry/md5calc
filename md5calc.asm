﻿.486
.MODEL  FLAT,STDCALL
OPTION  CASEMAP:NONE

incLUDE      windows.inc
incLUDE      kernel32.inc
incLUDELIB    kernel32.lib
incLUDE      user32.inc
incLUDELIB    user32.lib
include      comdlg32.inc
includelib    comdlg32.lib

.const
  
  IDC_BtFileName    equ  1001
  IDC_EditFileName  equ  1002
  IDC_EditMD5    equ  1003
  IDC_BtMD5    equ  1004
  IDC_BtTerminateMD5  equ  1005
  
.data
  
  szClassName    db  "Class",0
  szWindowName    db  "MD5加密算法",0
  szEdit      db  "Edit",0
  szButton    db  "Button",0

  szBtMD5      db  '开始MD5',0
  szBtTerminateMD5  db  '终止MD5',0
  szBtFileName    db  '文件...',0
  
  szStrFilter    db  "所有文件(*.*)",0,"*.*",0,0
  
  szCurrentFilePath  db  MAX_PATH dup  (?)
  szFileName      db   MAX_PATH dup (?)
  
  align  dword
  
  dwFLeftMove  db  07,12,17,22
  dwGLeftMove  db  05,09,14,20
  dwHLeftMove  db  04,11,16,23
  dwILeftMove  db  06,10,15,21
  
.data?

  hMD5Thd      dd  ?
  hInstance    dd  ?
  hWinMain    dd  ?
  
  hEditMD5    dd  ?
  hBtMD5      dd  ?
  hBtTerminateMD5    dd  ?

  hBtFileName    dd  ?
  hEditFileName    dd  ?
  lpFileMemHeader    dd  ?
  dwOpenFileSize    dd  ?
  

.code
_OpenFileProc       proc  uses ebx esi edi  ,_hWnd
  LOCAL  @dwOpenFileSizeHigh
  LOCAL  @szBuffer3[1024]:byte
  
  LOCAL  @stOFN:OPENFILENAME
  LOCAL  @stSA:SECURITY_ATTRIBUTES
  
  LOCAL  @hDC1

  LOCAL  @hFileMap
  LOCAL  @hOpenFile

  LOCAL  @dwTmp
  
  .if  lpFileMemHeader
    invoke  UnmapViewOfFile,lpFileMemHeader
    mov  lpFileMemHeader,0
  .endif
  
  invoke  SetWindowText,hEditFileName,0
  invoke  SetWindowText,hEditMD5,0
  mov  dwOpenFileSize,0
  
  invoke  RtlZeroMemory,addr @stOFN,sizeof @stOFN
  mov  @stOFN.lStructSize,sizeof @stOFN
  lea  eax,szCurrentFilePath
  mov  @stOFN.lpstrFile,eax
  mov  @stOFN.nMaxFile,MAX_PATH
  mov  @stOFN.Flags,OFN_FILEMUSTEXIST OR OFN_PATHMUSTEXIST
      
  invoke  GetOpenFileName,addr @stOFN  
  .if  !eax
    jmp  _NOPE
  .endif
  invoke  SetWindowText,hEditFileName,addr szCurrentFilePath
  
  invoke  RtlZeroMemory,addr @stSA,sizeof @stSA  
  invoke  CreateFile,addr szCurrentFilePath, GENERIC_READ,FILE_SHARE_DELETE or FILE_SHARE_READ or FILE_SHARE_WRITE,addr @stSA,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL ,0;FILE_FLAG_DELETE_ON_CLOSE or 
  .if  eax == INVALID_HANDLE_VALUE 
    jmp  _NOPE
  .endif
  mov  @hOpenFile,eax
  
  
  invoke  GetFileSize,@hOpenFile,addr @dwOpenFileSizeHigh
  mov  dwOpenFileSize,eax
  .if  !eax
    invoke  CloseHandle,@hFileMap
    invoke  CloseHandle,@hOpenFile
    jmp  _NOPE
  .endif 
  
  
  invoke  CreateFileMapping,@hOpenFile,0,PAGE_READONLY,0,0,0
  .if  !eax
    invoke  CloseHandle,@hOpenFile
    jmp  _NOPE
  .endif
  mov  @hFileMap,eax
  
  
  invoke  MapViewOfFile,@hFileMap, FILE_MAP_READ,0,0,0
  .if  !eax
    invoke  CloseHandle,@hFileMap
    invoke  CloseHandle,@hOpenFile
    jmp  _NOPE
    
  .endif
  mov  lpFileMemHeader,eax
  
  invoke  CloseHandle,@hFileMap
  invoke  CloseHandle,@hOpenFile
  
  _NOPE:  
  
  ret
_OpenFileProc endp

_MD5hashProc    proc  uses  edi esi,_lpFileMem,_dwFileSize,_lpszMD5Buffer
  local    @dwA,@dwB,@dwC,@dwD
  LOCAL  @dwRotateMD5Count
  LOCAL  @dwEndRotateMD5Count
  LOCAL  @szBuffer1[128]:byte
  
  LOCAL  @dwFTi[64]:dword
  
  LOCAL  @bEndMD5
  LOCAL  @dbFileSizeInBuffer:byte
  
  LOCAL  @dwTiCount
  LOCAL  @dwTiN0
  LOCAL  @dwTiN1
  LOCAL  @dwTiN2
  LOCAL  @wCW:word
  
  LOCAL  @dwTmp1
  LOCAL  @dwTmp2
  
  LOCAL  @dwFOffset
  LOCAL  @dwGOffset
  LOCAL  @dwHOffset
  LOCAL  @dwIOffset
      
      
      ;初始化FOffset表
      push  0
      pop  @dwFOffset
      push  1
      pop  @dwGOffset
      push  5
      pop  @dwHOffset
      push  0
      pop  @dwIOffset
      
      ;初始化Ti
      
      mov  @dwTiCount,1
      mov  @dwTiN1,10000000h
      mov  @dwTiN2,10h
      lea  edi,@dwFTi
      
      mov  @wCW,0001011111111111b;设置近似值为剪裁掉尾数状态
      wait
      fldcw  @wCW
    @@:  
      fild  @dwTiCount
      fsin  
      fabs
      
      fild  @dwTiN1
      fmul
      
      fist   @dwTmp1
      fild  @dwTmp1
      fsub
      
      fild  @dwTiN2
      fmul
      fistp   @dwTmp2
      fwait
      
      mov  eax,@dwTmp1
      shl  eax,4
      add  eax,@dwTmp2
      mov  dword ptr [edi],eax
       
      inc  @dwTiCount
      add  edi,sizeof dword
      
      cmp  @dwTiCount,65
      jz  @f
      jmp  @b  
    @@:    
          
      xor  al,al
      lea  edi,@szBuffer1
      mov  ecx,sizeof @szBuffer1
      cld
      rep  stosb
      
      mov  @bEndMD5,FALSE
      
      ;求得512取模及需要填充的数据
      mov  eax,_dwFileSize
      shr  eax,6
      mov  @dwRotateMD5Count,eax
      
      ;模64后末尾字符串复制到缓冲区
      lea  edi,@szBuffer1
      mov  esi,_lpFileMem
      mov    eax,_dwFileSize
      mov  ecx,eax
      and  eax,0ffffffc0h;
      add  esi,eax
  
      and  ecx,03fh;除以64求模
      mov  @dbFileSizeInBuffer,cl
      .if  ecx
        cld
        rep  movsb
      .endif
      mov  byte ptr [edi],080h;消息末尾附1
      
      ;后64位填充消息长度//转为处理@szBuffer1
      lea  edi,@szBuffer1
      
      mov  eax,_dwFileSize;ebx
      mov  edx,eax
      shl  eax,3
      shr  edx,29
      
      mov    cl,@dbFileSizeInBuffer;_dwFileSize
      .if   cl < 56
        mov  @dwEndRotateMD5Count,1
        mov    dword ptr [edi+ 60],edx
        mov    dword ptr [edi+ 56],eax
      .else
        mov  @dwEndRotateMD5Count,2
        mov  dword ptr [edi+124],edx
        mov    dword ptr [edi+120],eax
      .endif
      
      push  067452301h
      pop  @dwA
      push  0efcdab89h
      pop  @dwB
      push  098badcfeh
      pop  @dwC
      push  010325476h
      pop  @dwD
      
      mov  edi,_lpFileMem
      
    _Rotate:   
      mov  ecx, @dwRotateMD5Count
      .while  ecx
        push  ecx
        
        push  @dwD
        push  @dwC
        push  @dwB
        push  @dwA
        
        xor  esi,esi
        .while  esi < 64
          mov    eax,@dwB
          mov    ebx,@dwC
          mov    ecx,@dwD
          
          .if  esi < 16;F
            and    ebx,eax
            not    eax
            and    eax,ecx
            or    eax,ebx
            
            mov  ecx,@dwFOffset
            inc  @dwFOffset
          .elseif esi < 32;G
            and    eax,ecx
            not    ecx
            and    ecx,ebx
            or    eax,ecx
            
            mov  ecx,@dwGOffset
            add  @dwGOffset,5
          .elseif esi < 48;H
            xor    eax,ebx
            xor    eax,ecx
            
            mov  ecx,@dwHOffset
            add  @dwHOffset,3
          .else    ;I
            not    ecx
            or    eax,ecx
            xor    eax,ebx
            
            mov  ecx,@dwIOffset
            add  @dwIOffset,7
          .endif
        
          add  eax,@dwA
          
          ;dwFOffset
          and  ecx,0fh
          add    eax,dword ptr [edi+ ecx * sizeof dword];x
          
          lea  ecx,@dwFTi
          add    eax,dword ptr [ecx  + esi * sizeof dword];t
          
          mov  ecx,esi  
          shr  ecx,2
          and  ecx,12
          mov  edx,esi  
          and  edx,3
          mov  cl,byte ptr [offset dwFLeftMove + edx + ecx]
          rol    eax,cl
          add    eax,@dwB
          
          xchg  eax,@dwB
          xchg  eax,@dwC
          xchg  eax,@dwD
          xchg  eax,@dwA
          
          inc  esi
        .endw  
        
        pop  eax
        add  @dwA,eax
        pop  eax
        add  @dwB,eax
        pop  eax
        add  @dwC,eax
        pop  eax
        add  @dwD,eax
        
        add    edi,16*sizeof dword
        pop  ecx
        dec  ecx
      .endw  
      
      .if  @bEndMD5 == TRUE
        jmp  @f
      .endif
      
      push  @dwEndRotateMD5Count
      pop  @dwRotateMD5Count
      
      lea  edi,@szBuffer1
      mov  @bEndMD5,TRUE
      jmp  _Rotate
    @@:  
      
      mov  eax,@dwA
      bswap  eax
      mov  ebx,@dwB
      bswap  ebx
      mov  ecx,@dwC
      bswap  ecx
      mov  edx,@dwD
      bswap  edx
      
    jmp  @f
      szMD5Format@_MD5hashProc    db  '%.8x%.8x%.8x%.8x',0
    @@:
    invoke  wsprintf,_lpszMD5Buffer,addr szMD5Format@_MD5hashProc,eax,ebx,ecx,edx
  ret
_MD5hashProc    endp


_MD5Proc   proc  uses ebx edi esi ,_hWnd
    LOCAL    @szMD5Buffer[32]:byte
    LOCAL  @dwMemSize
    
  .if  ! lpFileMemHeader
    jmp  @f
      FailMessage@_MD5Proc  db  '创建文件镜像失败,无法计算MD5码！',0
    @@:
    invoke  SetWindowText,hEditMD5,addr FailMessage@_MD5Proc
    jmp  _NOPE  
  .endif
  
  invoke  _MD5hashProc,lpFileMemHeader,dwOpenFileSize, addr @szMD5Buffer
  invoke  SetWindowText,hEditMD5,addr @szMD5Buffer
  
  _NOPE:  
  
    ret
_MD5Proc  endp


_BackgroundWndProc      proc uses  ebx esi edi, _hWnd, uMsg:UINT, wParam:WPARAM, lParam:LPARAM 
 
  MOV  eax,uMsg  
  
  .if  eax == WM_COMMAND
    mov  eax,wParam
    
    .if  ax ==IDC_BtFileName
      invoke  CreateThread,0,0,addr   _OpenFileProc,0,0,0
      invoke  CloseHandle,eax  
    .elseif  ax ==   IDC_BtMD5
      invoke  CreateThread,0,0,addr   _MD5Proc,0,0,0
      invoke  CloseHandle,eax  
    .endif
  .elseif  eax==WM_CREATE 
  
    ;创建按键
    
    invoke  CreateWindowEx,0,addr szButton,addr szBtFileName,BS_DEFPUSHBUTTON or WS_CHILDWINDOW  or WS_TABSTOP or WS_VISIBLE  ,\;
      20,20,90,22,_hWnd,IDC_BtFileName,hInstance,0
      mov  hBtFileName,eax
    
    invoke  CreateWindowEx,0,addr szButton,addr szBtMD5,BS_DEFPUSHBUTTON or WS_CHILDWINDOW  or WS_TABSTOP or WS_VISIBLE  ,\;
      20,60,90,22,_hWnd,IDC_BtMD5,hInstance,0
      mov  hBtMD5,eax
    
    ;创建编辑框
    
    invoke  CreateWindowEx,WS_EX_CLIENTEDGE,addr szEdit ,0,ES_LEFT or WS_CHILD OR  WS_VISIBLE,\ 
      120,20,530,22,_hWnd,IDC_EditFileName,hInstance,0
    mov  hEditFileName,eax
    invoke  CreateWindowEx,WS_EX_CLIENTEDGE,addr szEdit ,0,ES_LEFT or WS_CHILD OR  WS_VISIBLE,\ 
      120,60,530,22,_hWnd,IDC_EditMD5,hInstance,0
    mov  hEditMD5,eax
    
  .elseif eax==WM_DESTROY
    .if  hMD5Thd
      invoke  TerminateThread,hMD5Thd,0
      invoke  CloseHandle,hMD5Thd
    .endif  
    
    invoke  PostQuitMessage,0
    invoke  DestroyWindow,hWinMain;_hWnd
    
  .else 
          invoke DefWindowProc,_hWnd,uMsg,wParam,lParam 
    ret 
  .endif 
  
  xor eax,eax 
  ret 
_BackgroundWndProc endp 

_WinMain       proc  uses esi edi ebx
  LOCAL  @stWc:WNDCLASSEX
  LOCAL  @stMsg:MSG
  LOCAL  @stRect1:RECT
  LOCAL  @stRect2:RECT
  LOCAL  @dwHigh
  
  invoke  RtlZeroMemory,addr @stWc,sizeof @stWc
  
  mov  @stWc.cbSize,sizeof @stWc
  mov  @stWc.style,CS_VREDRAW OR CS_HREDRAW OR CS_DBLCLKS ;;
  mov  @stWc.lpfnWndProc,offset _BackgroundWndProc

  mov  @stWc.lpszClassName,offset szClassName
  mov  @stWc.hbrBackground,COLOR_BTNFACE+2
  push  hInstance
  pop  @stWc.hInstance
  invoke  LoadIcon,hInstance,IDI_APPLICATION
  mov  @stWc.hIcon,eax
  mov  @stWc.hIconSm,eax
  invoke  LoadCursor,0,IDC_ARROW
  mov  @stWc.hCursor,eax
  
  invoke  RegisterClassEx,addr @stWc
  
  invoke  CreateWindowEx,WS_EX_WINDOWEDGE,addr szClassName,addr szWindowName,\
     WS_SYSMENU  or WS_DLGFRAME    or WS_BORDER  or WS_MINIMIZEBOX  OR DS_MODALFRAME or WS_VISIBLE,\
    100,100,750,200,0,0,hInstance,0
    mov  hWinMain,eax
      
  .while  TRUE
    invoke  GetMessage,addr @stMsg,0,0,0
    .break .if !eax
    invoke  TranslateMessage,addr @stMsg
    invoke  DispatchMessage,addr @stMsg
  .endw
  mov  eax,@stMsg.wParam

  ret
_WinMain endp

Start:

  invoke  GetModuleHandle,0
  mov  hInstance,eax
  invoke  _WinMain
  invoke  ExitProcess,0

end Start